export class CategoryList {
    public idCategory: string;
    public strCategory: string;
    public strCategoryThumb: string;
    public strCategoryDescription: string;

    constructor(id: string, category: string, thumb: string, desc: string) {
        this.idCategory = id;
        this.strCategory = category;
        this.strCategoryThumb = thumb;
        this.strCategoryDescription = desc;
    }

}