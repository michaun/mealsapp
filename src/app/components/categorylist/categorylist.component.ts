import { Component, EventEmitter, NgIterable, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CategoryList } from './categorylist.model';
import { ConnectionService } from '../../services/connection.service';
import { Meals } from '../../models/meals.model';
import { Categories } from '../../models/categories.model';

@Component({
  selector: 'app-categorylist',
  templateUrl: './categorylist.component.html',
  styleUrls: ['./categorylist.component.css']
})
export class CategorylistComponent implements OnInit {
  meals: Categories = {categories: {}};
  categoriesArr: CategoryList[] = [];
  currentRow: CategoryList[] = [];
  startCategory: number = 0;
  windowLenCategory: number = 3;

  constructor(private connectionService: ConnectionService, private router: Router) {}

  ngOnInit(): void {
    this.connectionService.getCategories().subscribe(
      (data: Categories) => {
        this.meals = {...data};
        this.categoriesArr = Object.values(this.meals.categories);
        this.currentRow = this.getSliceCategories(this.startCategory,this.windowLenCategory);
      }
    );
  }

  getSliceCategories(start: number, len: number) {
    return [
      ...this.categoriesArr.slice(start),
      ...this.categoriesArr.slice(0, start),
    ].slice(0, len);
  }

  moveWindow(direction:number) {
    this.startCategory = (this.startCategory + (direction * this.windowLenCategory)) % this.categoriesArr.length;
    this.currentRow = this.getSliceCategories(this.startCategory,this.windowLenCategory);
  }


}
