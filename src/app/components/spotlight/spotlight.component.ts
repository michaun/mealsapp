import { Component, OnInit } from '@angular/core';
import { Categories } from 'src/app/models/categories.model';
import { Item } from 'src/app/models/item.model';
import { Meal } from 'src/app/models/meal.model';
import { Meals } from 'src/app/models/meals.model';
import { ConnectionService } from 'src/app/services/connection.service';
import { CategoryList } from '../categorylist/categorylist.model';

@Component({
  selector: 'app-spotlight',
  templateUrl: './spotlight.component.html',
  styleUrls: ['./spotlight.component.css']
})
export class SpotlightComponent implements OnInit {
  categoriesArr: CategoryList[] = [];
  mealsArr: Meal[] = [];
  categories: Categories = {categories: {}};
  meals: Meals = {meals: {}};
  itemsArr: Item[] = [];
  items: Meals = {meals: {}};
  randomCategory: string = '';
  randomMeal: string = '';
  mealName: string = '';
  mealImage: string = '';
  mealId: string = '';

  constructor(private connectionService: ConnectionService) {}

  ngOnInit(): void {
    this.connectionService.getCategories().subscribe(
      (data: Categories) => {
        this.categories = {...data};
        this.categoriesArr = Object.values(this.categories.categories);
        console.log(this.categoriesArr);
        let a = Math.floor(Math.random() * this.categoriesArr.length);
        console.log(a);
        this.randomCategory = this.categoriesArr[a].strCategory;
        this.connectionService.getMealsByType(this.randomCategory).subscribe(
          (data: Meals) => {
            this.meals = {...data};
            this.mealsArr = Object.values(this.meals.meals);
            console.log(this.meals)
            let a = Math.floor(Math.random() * this.mealsArr.length);
            this.randomMeal = this.mealsArr[a].idMeal;
            this.connectionService.getDetailsById(this.randomMeal).subscribe(
              (data: Meals) => {
                this.items = {...data};
                this.itemsArr = Object.values(this.items.meals);
                for (let item in this.itemsArr) {
                  this.mealName = this.itemsArr[item].strMeal;
                  this.mealImage = this.itemsArr[item].strMealThumb;
                  this.mealId = this.itemsArr[item].idMeal;
                }
              }
            );
        
          }
        );
    
      }
    );
  }

}
