import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MeallistComponent } from './meallist.component';

describe('CategoryComponent', () => {
  let component: MeallistComponent;
  let fixture: ComponentFixture<MeallistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MeallistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MeallistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
