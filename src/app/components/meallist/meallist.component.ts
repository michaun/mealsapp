import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Meal } from '../../models/meal.model';
import { ConnectionService } from '../../services/connection.service';
import { Meals } from '../../models/meals.model';

@Component({
  selector: 'app-meallist',
  templateUrl: './meallist.component.html',
  styleUrls: ['./meallist.component.css']
})
export class MeallistComponent implements OnInit {
  mealsArr: Meal[] = [];
  meals: Meals = {meals: {}};

  constructor(private connectionService: ConnectionService, private router: Router, private route: ActivatedRoute) {}

  ngOnInit(): void {
    const mealName : string = this.route.snapshot.params['category'];
    console.log(mealName)
    this.connectionService.getMealsByType(mealName).subscribe(
      (data: Meals) => {
        this.meals = {...data};
        this.mealsArr = Object.values(this.meals.meals);
        console.log(this.meals)
      }
    );
  }

}
