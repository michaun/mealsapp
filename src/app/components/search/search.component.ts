import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Meal } from 'src/app/models/meal.model';
import { Meals } from 'src/app/models/meals.model';
import { ConnectionService } from 'src/app/services/connection.service';
import { SearchService } from 'src/app/services/search.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  mealsArr: Meal[] = [];
  meals: Meals = {meals: {}};
  searchVal: string = '';
  searched: boolean = false;

  constructor(private searchService: SearchService, private router: Router, private route: ActivatedRoute) {}

  ngOnInit(): void {}

  searchForItem() {
    console.log(this.searchVal);
    let mealName = this.searchVal;
    this.searchService.getMealsByName(mealName).subscribe(
      (data: Meals) => {
        this.meals = {...data};
        this.mealsArr = Object.values(this.meals.meals);
        console.log(this.meals)
        this.searched = true;
      }
    );
  }

}
