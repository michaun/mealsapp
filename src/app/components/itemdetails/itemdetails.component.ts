import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ConnectionService } from '../../services/connection.service';
import { Item } from '../../models/item.model';
import { Meals } from '../../models/meals.model';

@Component({
  selector: 'app-itemdetails',
  templateUrl: './itemdetails.component.html',
  styleUrls: ['./itemdetails.component.css']
})
export class ItemdetailsComponent implements OnInit {
  itemsArr: Item[] = [];
  items: Meals = {meals: {}};

  constructor(private connectionService: ConnectionService, private router: Router, private route: ActivatedRoute) {}

  ngOnInit(): void {
    const itemId = this.route.snapshot.params['id'];
    this.connectionService.getDetailsById(itemId).subscribe(
      (data: Meals) => {
        this.items = {...data};
        this.itemsArr = Object.values(this.items.meals);
        for (let item in this.itemsArr) {
          console.log(this.itemsArr[item].strYoutube);
          this.itemsArr[item].strYoutube = this.itemsArr[item].strYoutube.replace('/watch?v=','/embed/')
          console.log(this.itemsArr[item].strYoutube);
        }
      }
    );
  }

}
