import { Component, Input, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-videoembed',
  templateUrl: './videoembed.component.html',
  styleUrls: ['./videoembed.component.css']
})
export class VideoembedComponent implements OnInit {
  @Input() url: string = '';
  safeUrl : SafeResourceUrl = '';

  constructor(private sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    console.log(this.url);
    this.safeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
  }

}
