export class Meal {
    public idMeal: string;
    public strMeal: string;
    public strMealThumb: string;

    constructor(id: string, category: string, thumb: string, desc: string) {
        this.idMeal = id;
        this.strMeal = category;
        this.strMealThumb = thumb;
    }

}