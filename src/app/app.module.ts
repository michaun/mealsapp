import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { MeallistComponent } from './components/meallist/meallist.component';
import { CategorylistComponent } from './components/categorylist/categorylist.component';
import { FooterComponent } from './components/footer/footer.component';
import { ItemdetailsComponent } from './components/itemdetails/itemdetails.component';
import { RouterModule, Routes } from '@angular/router';
import { ConnectionService } from './services/connection.service';
import { HttpClientModule } from '@angular/common/http';
import { VideoembedComponent } from './components/videoembed/videoembed.component';
import { SpotlightComponent } from './components/spotlight/spotlight.component';
import { MainComponent } from './components/main/main.component';
import { SearchComponent } from './components/search/search.component';
import { FormsModule } from '@angular/forms';
import { SearchService } from './services/search.service';

const appRoutes: Routes = [
  { path: "", component: MainComponent},
//  { path: "categories", component: CategoryComponent},
{ path: "meallist/:category", component: MeallistComponent},
{ path: "itemdetails/:id", component: ItemdetailsComponent},
{ path: "search", component: SearchComponent},
//{ path: "**", component: MainComponent}

]

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MeallistComponent,
    CategorylistComponent,
    FooterComponent,
    ItemdetailsComponent,
    VideoembedComponent,
    SpotlightComponent,
    MainComponent,
    SearchComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    FormsModule
  ],
  providers: [ConnectionService, SearchService],
  bootstrap: [AppComponent]
})
export class AppModule { }
