import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Meals } from '../models/meals.model';

@Injectable()
export class SearchService {

  constructor(private http: HttpClient) {}

  getMealsByName(name: string) {
    return this.http.get<Meals>('https://www.themealdb.com/api/json/v1/1/search.php?s='+name);
  }
}
