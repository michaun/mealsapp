import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Categories } from '../models/categories.model';
import { Meals } from '../models/meals.model';

@Injectable()
export class ConnectionService {
  veganMeals : Meals = {meals: {}};

  constructor(private http: HttpClient) {}

  getCategories() {
    return this.http.get<Categories>('https://www.themealdb.com/api/json/v1/1/categories.php');
  }

  getMealsByType(type: string) {
    return this.http.get<Meals>('https://www.themealdb.com/api/json/v1/1/filter.php?c='+type);
  }

  getDetailsById(id: string) {
    return this.http.get<Meals>('https://www.themealdb.com/api/json/v1/1/lookup.php?i='+id);
  }
}
